# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Include MDEs .bashrc_exports if it exists
if [ -f /etc/mde/dot/.bashrc_exports ]; then
    . /etc/mde/dot/.bashrc_exports
fi
# Include MDEs .bashrc_aliases if it exists
if [ -f /etc/mde/dot/.bashrc_aliases ]; then
    . /etc/mde/dot/.bashrc_aliases
fi

PS1='\[\033[01;31m\]\u@\h\[\033[0m\]:\[\033[01;34m\]\w\[\033[00m\]$ '

