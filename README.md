# MinimalDE
Welcome to my Desktop Environment. An accumulation of my favorite Window Manager, Terminal Emulator, other programs and configs.

![Screenshot](screenshots/maim_2024-02-22-17:11:21.354748373.png)

Following the goals of this DE:
- Minimal workload for HW
- Focus on Keyboard / Terminal
- Minimal Distraction
- Maximum Productiveness
- Subtle Animations and Minimalistic Design

## Clone
This project makes use of submodules, so please clone recursively.

- https://github.com/alacritty/alacritty (v0.13.1)
- https://gitlab.com/MediumRar/ml (v0.1.1)
- https://github.com/polybar/polybar (3.7.1)
- https://github.com/qtile/qtile (v0.24.0)
- https://gitlab.com/MediumRar/slock (mde)
- https://github.com/jstkdng/ueberzugpp (v2.9.2)
- https://github.com/sxyazi/yazi (v0.2.2)

```
git clone --recurse-submodules https://gitlab.com/MediumRar/mde
```

## Installation
There is an installation bash-script, which is configured for Arch Linux per default. If you are using a different OS, you'll have to adapt the dependency commands and maybe some paths depending on how exotic your OS is. Whatever OS you are using though, you will have to do some minimal configuration to the script and are encouraged to check what it does before executing it.

## Configuration
The installation script will automatically attempt to install all configuration files needed. If one exists already, the script will ask you if you want to override the existing one or not. The script will also preserve the rights of the files being copied into the user home-directory in a effort to end up with the proper access right, however, that very much depends on how you checked out this branch obviously. I don't care. Worst case is you have to change some user rights if you want to adapt the configs.

### Autostart
Session managers are now supported by a .desktop file installed at ```/usr/share/xsessions```. Please note though that this DE does not come with a session manager, since I don't like any of them and don't see the point. If you are not using any session managers like me, copy the following to the end of your .bashrc to autostart whatever you have in your .xinitirc ( hopefully MDE :) ).
```
# autostartX
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
    exec startx
fi
```

### MDE .bashrc
To make usage of MDEs .bashrc, copy the following to the very top of your .bashrc:

#### Root
```
# Include MDEs .bashrc if it exists
if [ -f /etc/mde/dot/root/.bashrc ]; then
    . /etc/mde/dot/root/.bashrc
fi
```

#### User
```
# Include MDEs .bashrc if it exists
if [ -f /etc/mde/dot/user/.bashrc ]; then
    . /etc/mde/dot/user/.bashrc
fi
```

### Wallpaper
To set a wallpaper copy any image to ```${HOME}/.local/share/wallpaper``` where wallpaper is the actual file.

## Usage
Some useful keyboard shortcuts generally available and/or specific to this DE.

### MDE
- Spawn any Program ```super+S```
- Spawn Terminal ```super+T & super+Return```
- Spawn Command Line File Browser ```super+y```
- Spawn File Browser ```super+F```
- Close active Pogram ```super+Q```
- Lock Screen ```super+L```
- Navigate between windows ```super+(Left,Right,Down,Up)```
- Move windows ```super+shift+(Left,Right,Down,Up)```
- Resize windows ```super+ctrl+(Left,Right,Down,Up)```
- Toggle column stack ```super+shift+Return```
- Toggle fullscreen ```super+Tab```
- Navigate between workspaces ```super+(1-9)```
- Move windows between workspaces ```super+shift+(1-9)```
- Screenshot ```super+F12```
- Selective screenshot ```super+shift+F12```

### WWW & File - Browser
- Go to URL ```alt+D```
- Go back ```alt+Left```
- Go forward ```alt+Right```
- Go to next tab ```ctrl+PageDown```
- Go to previous tab ```ctrl+PageUp```
- Close Tab ```ctrl+W```
- Close Application ```super+Q```

