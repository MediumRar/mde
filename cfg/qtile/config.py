# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile import bar, layout, qtile, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

# Super (Windows) key
_mde_mod = "mod4"

_mde_color_background =     "#090909AA"
_mde_color_inactive =       "#333333"
_mde_color_active_block =   "#CC00CC"
_mde_color_active_float =   "#00AAAA"
_mde_color_active_stack =   "#FF9900"

_mde_useless_gap = 3

_mde_screenshot_dir = "~/Pictures/screenshots"
_mde_screenshot_files = "$(date +'maim_%Y-%m-%d-%H:%M:%S.%N.png')"
_mde_screenshot_path = _mde_screenshot_dir + "/" + _mde_screenshot_files

@lazy.function
def goto_current_screen_group(qtile, group):
    active_screen = qtile.current_screen
    active_screen_index = active_screen.info()["index"]
    active_screen.toggle_group("%s%s" % (active_screen_index, group))

@lazy.function
def move_window_to_current_screen_group(qtile, group):
    active_screen = qtile.current_screen
    active_screen_index = active_screen.info()["index"]
    qtile.move_to_group("%s%s" % (active_screen_index, group))

@lazy.function
def move_window_to_prev_screen(qtile):
    old_active_window = qtile.current_window
    qtile.prev_screen()
    newly_active_group = qtile.current_group
    old_active_window.togroup(newly_active_group.name)

@lazy.function
def move_window_to_next_screen(qtile):
    old_active_window = qtile.current_window
    qtile.next_screen()
    newly_active_group = qtile.current_group
    old_active_window.togroup(newly_active_group.name)

# Called when the output configuration is changed (e.g. via randr in X11).
# If you have reconfigure_screens = True in your config then qtile will
# automatically reconfigure your screens when it detects a change to the
# screen configuration. This hook is fired before that reconfiguration takes
# place. The screens_reconfigured hook should be used where you want to
# trigger an event after the reconfiguration.
#@hook.subscribe.screen_change
#def screen_change(event):
# TODO: We should delete all groups of all screens but screen 0 i guess?

# Super (Windows) key
mod = _mde_mod

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod],              "Left",     lazy.layout.left(),                         desc="Move focus to left"),
    Key([mod],              "Right",    lazy.layout.right(),                        desc="Move focus to right"),
    Key([mod],              "Down",     lazy.layout.down(),                         desc="Move focus down"),
    Key([mod],              "Up",       lazy.layout.up(),                           desc="Move focus up"),
    Key([mod],              "space",    lazy.layout.next(),                         desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"],     "Left",     lazy.layout.shuffle_left(),                 desc="Move window to the left"),
    Key([mod, "shift"],     "Right",    lazy.layout.shuffle_right(),                desc="Move window to the right"),
    Key([mod, "shift"],     "Down",     lazy.layout.shuffle_down(),                 desc="Move window down"),
    Key([mod, "shift"],     "Up",       lazy.layout.shuffle_up(),                   desc="Move window up"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"],     "Return",   lazy.layout.toggle_split(),                 desc="Toggle between split and unsplit sides of stack"),

    # Grow windows. If curre"space",nt window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"],   "Left",     lazy.layout.grow_left(),                    desc="Grow window to the left"),
    Key([mod, "control"],   "Right",    lazy.layout.grow_right(),                   desc="Grow window to the right"),
    Key([mod, "control"],   "Down",     lazy.layout.grow_down(),                    desc="Grow window down"),
    Key([mod, "control"],   "Up",       lazy.layout.grow_up(),                      desc="Grow window up"),
    Key([mod, "control"],   "Return",   lazy.layout.normalize(),                    desc="Reset all window sizes"),

    Key([mod],              "Page_Up",  lazy.prev_screen(),                         desc="Reset all window sizes"),
    Key([mod],              "Page_Down",lazy.next_screen(),                         desc="Reset all window sizes"),
    Key([mod, "shift"],     "Page_Up",  move_window_to_prev_screen(),               desc="Reset all window sizes"),
    Key([mod, "shift"],     "Page_Down",move_window_to_next_screen(),               desc="Reset all window sizes"),

#    Key([mod],              ",",        lazy.prev_screen(),                         desc="Reset all window sizes"),
#    Key([mod],              ".",        lazy.next_screen(),                         desc="Reset all window sizes"),
# Doesn't work for whatever reason
# Apparetnly they are actually fucking missing:
# https://github.com/qtile/qtile/blob/master/libqtile/backend/x11/xkeysyms.py
#
#2024-03-01 14:29:11,697 ERROR libqtile manager.py:load_config():L117 Configuration error:
#Traceback (most recent call last):
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/core/manager.py", line 115, in load_config
#    self.config.validate()
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/confreader.py", line 154, in validate
#    raise ConfigError("No such key: %s" % k.key)
#libqtile.confreader.ConfigError: No such key: ,
#2024-03-01 14:29:11,710 ERROR libqtile manager.py:process_key_event():L454 KB command error reload_config: Traceback (most recent call last):
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/backend/x11/core.py", line 494, in lookup_key
#    keysym = xcbq.get_keysym(key.key)
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/backend/x11/xcbq.py", line 765, in get_keysym
#    raise XCBQError("Unknown key: %s" % key)
#libqtile.backend.x11.xcbq.XCBQError: Unknown key: ,
#
#During handling of the above exception, another exception occurred:
#
#Traceback (most recent call last):
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/command/interface.py", line 317, in call
#    return SUCCESS, cmd(obj, *args, **kwargs)
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/core/manager.py", line 301, in reload_config
#    self.load_config()
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/core/manager.py", line 145, in load_config
#    self.grab_key(key)
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/core/manager.py", line 476, in grab_key
#    syms = self.core.grab_key(key)
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/backend/x11/core.py", line 503, in grab_key
#    keysym, modmask = self.lookup_key(key)
#  File "/opt/mde/mde-venv/lib/python3.10/site-packages/libqtile/backend/x11/core.py", line 497, in lookup_key
#    raise utils.QtileError(err)
#libqtile.utils.QtileError: Unknown key: ,

    # Other system commands
    Key([mod],              "s",        lazy.spawn("ml"),                           desc="Spawn a command using a prompt widget"),
    Key([mod],              "q",        lazy.window.kill(),                         desc="Kill focused window"),
# Doesn't work with work laptop for whatever reason
#    Key([mod],              "l",        lazy.spawn("slock"),                        desc="Kill focused window"),
    Key([mod],              "escape",   lazy.shutdown(),                            desc="Shutdown Qtile"),
    Key([mod],              "Tab",      lazy.next_layout(),                         desc="Toggle between layouts"),
    Key([mod, "control"],   "r",        lazy.reload_config(),                       desc="Reload the config"),

    # Media Control
    Key([],     "XF86AudioPlay",        lazy.spawn("mde-mediactl -c playpause"),    desc=""),
    Key([],     "XF86AudioPrev",        lazy.spawn("mde-mediactl -c previous"),     desc=""),
    Key([],     "XF86AudioNext",        lazy.spawn("mde-mediactl -c next"),         desc=""),
    Key([],     "XF86AudioMute",        lazy.spawn("mde-audioctl -m"),              desc="Toggle Mute"),
    Key([],     "XF86AudioLowerVolume", lazy.spawn("mde-audioctl -v 1%-"),          desc="Lower Volume by 1%"),
    Key([],     "XF86AudioRaiseVolume", lazy.spawn("mde-audioctl -v 1%+"),          desc="Raise Volume by 1%"),

    Key([mod],  "F1",                   lazy.spawn("mde-audioctl -m"),              desc="Toggle Mute"),
    Key([mod],  "F2",                   lazy.spawn("mde-audioctl -v 1%-"),          desc="Lower Volume by 1%"),
    Key([mod],  "F3",                   lazy.spawn("mde-audioctl -v 1%+"),          desc="Raise Volume by 1%"),
    Key([mod],  "F4",                   lazy.spawn("mde-audioctl -v 100%"),         desc="Set Volume to 100%"),

    # Spawn specific programs
    Key([mod],              "t",        lazy.spawn("alacritty"),                    desc="Launch Terminal"),
    Key([mod],              "Return",   lazy.spawn("alacritty"),                    desc="Launch Terminal"),
    Key([mod],              "y",        lazy.spawn("alacritty -e yazi"),            desc="Launch CL-File-Browser"),
    Key([mod],              "f",        lazy.spawn("pcmanfm"),                      desc="Launch File-Browser"),
    Key([mod],              "F12",      lazy.spawn("maim " + _mde_screenshot_path, shell=True),      desc="Take Screenshot"),
    Key([mod, "shift"],     "F12",      lazy.spawn("maim -su " + _mde_screenshot_path, shell=True),   desc="Take selective Screenshot"),
]

# Add key bindings to switch VTs in Wayland.
# We can't check qtile.core.name in default config as it is loaded before qtile is started
# We therefore defer the check until the key binding is run by using .when(func=...)
for vt in range(1, 8):
    keys.append(
        Key(
            ["control", "mod1"],
            f"f{vt}",
            lazy.core.change_vt(vt).when(func=lambda: qtile.core.name == "wayland"),
            desc=f"Switch to VT{vt}",
        )
    )

groups = []
for i in range(0,2+1):
    for j in range(1,9+1):
        groups.append(Group(
            name="%s%s" % (i,j),
            screen_affinity=i,
            position=j,
            label="Screen%sGroup%s" % (i,j),
        ))

for gid in range(1,9+1):
    keys.extend([
        # mod4 + group number = switch to group on active screen
        Key(
            [mod],
            str(gid),
            goto_current_screen_group(gid),
            desc="Switching group",
        ),
        # mod4 + shift + group number = move focused window to group on active screen
        Key(
            [mod, "shift"],
            str(gid),
            move_window_to_current_screen_group(gid),
            desc="Move focused window",
        ),
    ])

layouts = [
    layout.Columns(
        #align=layout.Columns._left,
        border_focus=_mde_color_active_block,
        border_normal=_mde_color_inactive,
        border_focus_stack=[_mde_color_active_stack],
        border_normal_stack=[_mde_color_active_stack],
        border_width=_mde_useless_gap,
        border_on_single=False,
        single_border_width=None,
        margin=_mde_useless_gap,
        margin_on_single=_mde_useless_gap*2,
        grow_amount=5),
    layout.Max(
        margin=_mde_useless_gap*2),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    foreground="#DDDDDD",
    font="sans",
    fontsize=16,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        left=bar.Gap(_mde_useless_gap),
        right=bar.Gap(_mde_useless_gap),
        top=bar.Gap(_mde_useless_gap),
        bottom=bar.Gap(_mde_useless_gap),
        # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        x11_drag_polling_rate = 60,
    ),
    Screen(
        left=bar.Gap(_mde_useless_gap),
        right=bar.Gap(_mde_useless_gap),
        top=bar.Gap(_mde_useless_gap),
        bottom=bar.Gap(_mde_useless_gap),
        # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        x11_drag_polling_rate = 60,
    ),
    Screen(
        left=bar.Gap(_mde_useless_gap),
        right=bar.Gap(_mde_useless_gap),
        top=bar.Gap(_mde_useless_gap),
        bottom=bar.Gap(_mde_useless_gap),
        # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        x11_drag_polling_rate = 60,
    ),
]

# Button1 -> Left
# Button2 -> Wheel
# Button3 -> Right
mouse = [
    Click([mod],    "Button1", lazy.window.bring_to_front()),
    Drag([mod],     "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Click([mod],    "Button2", lazy.window.toggle_floating()),
    Drag([mod],     "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    border_focus=_mde_color_active_float,
    border_normal=_mde_color_inactive,
    border_width=2,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "MDE"
